<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<!--
/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. 
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 	
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.

 * CLIMDEX Project Group provides access to this code for bona fide
 * scientific research and personal usage only.

 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow 
 * researchers. Everyone is welcome to use the code, and we aim to make it 
 * reliable, but it does not have the same level of support as an operational 
 * service.
 */
-->
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="content-type"
	content="application/xhtml+xml; charset=UTF-8" />
<meta name="description" content="SEWOCS: Station CLIMDEX data" />
<meta name="keywords"
	content="sewocs,climdex,STATDEX,GHCN-Daily Stations,extremes,climate,climate extremes,climate indices,precipitation,temparature,etccdi,climate change detection" />
<title>Station statistics for climate extremes index (CLIMDEX)</title>
<s:head />

<!-- pageContext.request.contextPath is needed because this page is <object> and used in IE -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/styles/style.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/openlayers/theme/default/style.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/openlayers/theme/default/scalebar-fat.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/styles/map-jsp.css"
	type="text/css" />

<script
	src="${pageContext.request.contextPath}/openlayers/OpenLayers.js"
	type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/scripts/map.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/scripts/checkDataSource.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/scripts/checkSeason.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/scripts/checkYears.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/scripts/checkSubmit.js"
	type="text/javascript"></script>
<!--<script src="${pageContext.request.contextPath}/JSON/json2.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/JSON/cycle.js"
	type="text/javascript"></script>-->

<script type="text/javascript">
	/* This script contains Struts2 action scope varaible reference. 
	It does not work in script files but it works on JSP pages */
	/* The strings can be parsed as JOSN strings by using JSON library */
	var datasetNameJSArrayStr = '${datasetOptions.datasetNameJSArray}';
	var startYearJSArrayStr = '${datasetOptions.startYearJSArray}';
	var endYearJSArrayStr = '${datasetOptions.endYearJSArray}';
	var layerNameJSArrayStr = '${datasetOptions.layerNameJSArray}';
	var styleNameJSArrayStr = '${datasetOptions.styleNameJSArray}';

	var datasetNameJSArrayStrEvalStr = 'var datasetNameJSArray = '
			+ datasetNameJSArrayStr + ";";
	var startYearJSArrayStrEvalStr = 'var startYearJSArray = '
			+ startYearJSArrayStr + ";";
	var endYearJSArrayStrEvalStr = 'var endYearJSArray = ' + endYearJSArrayStr
			+ ";";
	var layerNameJSArrayStrEvalStr = 'var layerNameJSArray = '
			+ layerNameJSArrayStr + ";";
	var styleNameJSArrayStrEvalStr = 'var styleNameJSArray = '
			+ styleNameJSArrayStr + ";";

	eval(datasetNameJSArrayStrEvalStr);
	eval(startYearJSArrayStrEvalStr);
	eval(endYearJSArrayStrEvalStr);
	eval(layerNameJSArrayStrEvalStr);
	eval(styleNameJSArrayStrEvalStr);
	; // don't remove the semicolon on the left. it is needed.
</script>

<script type="text/javascript">
	"user strict";

	var currentDataset = "";
	/*
	 * init() function initialises the map with the default dataset, set in the request.
	 * It also initialises the previosDataset as the currentDataset.
	 */
	function loadPage()
	{
		var datasetSelect = document
				.getElementById("myForm_stationdexRequest_dataSource");

		var datasetSelectIndex = datasetSelect.selectedIndex;
		var dataset = datasetSelect[datasetSelectIndex].value;
		currentDataset = dataset;

		var startYear = '${stationdexRequest.startYear}';
		var endYear = '${stationdexRequest.endYear}';
		resetStartEndYears(startYear, endYear, dataset);

		/* mapCenter and mapZoom are sent to the server
		in order to preserve the values between multiple loadings of the page */
		var mapCenter = '${stationdexRequest.mapCenter}';
		var mapZoom = '${stationdexRequest.mapZoom}';
		createMap(mapCenter, mapZoom, dataset);
	}

	function getPreviousDataset(nextDataset)
	{
		var previousDS = null;
		var keys = Object.keys(datasetNameJSArray);

		var len = keys.length;
		var maxIndex = len - 1;
		if (len == 1)
		{
			previousDS = null;
			return;
		}

		var i = 0;
		for (i = 0; i < len; i++)
		{
			if (keys[i] == currentDataset) break;
		}
		var previousIndex = i - 1;
		if (previousIndex < 0) previousIndex = maxIndex;
		previousDS = keys[previousIndex];
		return previousDS;
	}

	function clearErrMessage()
	{
		var errorMsgElement = document.getElementById("errorMsg");
		// errorMsgElement may not exist when there was no error before
		if (errorMsgElement)
		{
			errorMsgElement.innerHTML = "";
		}
	}
</script>
</head>
<body id="bd" onload="loadPage()">
	<div id="ja-wrapper">
		<p>
			Select <b>Data Source</b> to change dataset. Click a station on the
			map and select parameters. Click OK to request the desired output.
		</p>
		<table class="maptable">
			<tr>
				<td id="leftcolumn">
					<div id="maparea">
						<div id="map" class="smallmap"></div>
						<div id="mouseloc">longitude, latitude</div>
						<div id="note">
							<p>
								<!-- TODO: This message should change, depending on the data set and the date of the source data. -->
								<b>*</b> Station locations are based on the data provided to us.
								Please note that station coordinates may not be as accurate as
								the map precision.
							</p>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td id="rightcolumn">
					<div id="station_form">
						<s:form id="myForm" name="mayForm" method="get" theme="simple"
							action="stationdex" validate="false"
							onsubmit="return checkSubmit(this)" accept-charset="UTF-8">
							<fieldset id="fields">
								<div id="dataset">
									<div class="field">
										<s:label key="label.stationdexRequest.dataSource" />
										<s:select key="stationdexRequest.dataSource"
											list="stationdexOptions.dataSourceList" listKey="key"
											listValue="value"
											onchange="clearErrMessage();return checkDataSource(this)" />
									</div>
								</div>

								<div id="station">
									<div class="field">
										<s:label key="label.stationdexRequest.stationId" />
										<s:textfield key="stationdexRequest.stationId" readonly="true" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.stationName" />
										<s:textfield key="stationdexRequest.stationName"
											readonly="true" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.latitude" />
										<s:textfield key="stationdexRequest.latitude" readonly="true" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.longitude" />
										<s:textfield key="stationdexRequest.longitude" readonly="true" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.altitude" />
										<s:textfield key="stationdexRequest.altitude" readonly="true" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.active" />
										<s:textfield key="stationdexRequest.active" readonly="true" />
									</div>
									<div class="field">
										<!--<s:label key="label.stationdexRequest.mapCenter" />-->
										<s:hidden key="stationdexRequest.mapCenter" />
									</div>
									<div class="field">
										<!--<s:label key="label.stationdexRequest.mapZoom" />-->
										<s:hidden key="stationdexRequest.mapZoom" />
									</div>
								</div>

								<div id="params">
									<div class="field">
										<s:label key="label.stationdexRequest.climateIndex" />
										<s:select key="stationdexRequest.climateIndex"
											list="stationdexOptions.climateIndexList" listKey="key"
											listValue="value"
											onchange="clearErrMessage();return checkSeasonType(this)" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.startYear" />
										<s:select key="stationdexRequest.startYear"
											list="stationdexOptions.startYearList"
											onchange="clearErrMessage();return checkEndYear(this)" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.endYear" />
										<s:select key="stationdexRequest.endYear"
											list="stationdexOptions.endYearList"
											onchange="clearErrMessage();return checkStartYear(this)" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.season" />
										<s:select key="stationdexRequest.season"
											list="stationdexOptions.seasonList" listKey="key"
											listValue="value" onchange="clearErrMessage();return true;" />
									</div>
									<div class="field">
										<s:label key="label.stationdexRequest.outputType" />
										<s:select key="stationdexRequest.outputType"
											list="stationdexOptions.outputTypeList" listKey="key"
											listValue="value" onchange="clearErrMessage();return true" />
									</div>
								</div>

								<div id="submitarea">
									<div id="submitbutton">
										<s:submit class="submit" value="OK"
											onclick="clearErrMessage();return true" />
									</div>
								</div>

							</fieldset>
						</s:form>
					</div>
				</td>
			</tr>
		</table>
		
		<div>
			&nbsp;&nbsp;&nbsp;You can download all station index data in compressed
			format by using links below: (300MB-400MB)<br />
			<div style="position: relative; left: 50pt">
				<!-- Ref: http://struts.apache.org/release/2.3.x/docs/iterator.html -->
				<!-- Ref: http://www.tutorialspoint.com/spring/spring_injecting_collection.htm -->
				<s:iterator status="status" value="datasetOptions.archiveFileUrlMap">
					<a href="<s:property value='value' />"><s:property value="key" /></a>
					<br />
				</s:iterator>
			</div>
		</div>

		<s:if
			test="hasFieldErrors() || hasActionMessages() || hasActionErrors()">
			<h6>
				<s:actionmessage id="errorMsg" />
				<s:fielderror id="errorMsg" />
				<s:actionerror id="errorMsg" />
			</h6>
		</s:if>
	</div>
</body>
</html>
