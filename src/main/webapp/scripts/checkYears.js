/*
 * checkYears.js
 */

"use strinct";

var startYearSelectId = "myForm_stationdexRequest_startYear";
var endYearSelectId = "myForm_stationdexRequest_endYear";

function checkEndYear(startYearSelect)
{
	clearErrMessage();
	var startYearSelect = document.getElementById(startYearSelectId);
	var endYearSelect = document.getElementById(endYearSelectId);
	// clearErrorMsg();
	var startYearIndex = startYearSelect.selectedIndex;
	var endYearIndex = endYearSelect.selectedIndex;
	// alert("start="+startYearIndex+" end="+endYearIndex);
	if (startYearIndex > endYearIndex)
	{
		alert("Start Year must be the same or earlier than End Year.");
		endYearSelect.selectedIndex = startYearIndex;
	}
	return true;
}

function checkStartYear(endYearSelect)
{
	clearErrMessage();
	var startYearSelect = document.getElementById(startYearSelectId);
	var endYearSelect = document.getElementById(endYearSelectId);
	// clearErrorMsg();
	var endYearIndex = endYearSelect.selectedIndex;
	var startYearIndex = startYearSelect.selectedIndex;
	// alert("start="+startYearIndex+" end="+endYearIndex);
	if (startYearIndex > endYearIndex)
	{
		alert("End Year must be the same or later than Start Year");
		startYearSelect.selectedIndex = endYearIndex;
	}
	return true;
}

/**
 * It is necessary to reset the startYear and endYear
 * if they have some values. This is because, they have to be
 * reset after the page is returned by an error.
 * @param startYear
 * @param endYear
 * @param dataset
 */
function resetStartEndYears(startYear, endYear, dataset)
{
	var startYearSelect = document.getElementById(startYearSelectId);
	var endYearSelect = document.getElementById(endYearSelectId);
	var yearMin = Number(startYearJSArray[dataset]);
	var yearMax = Number(endYearJSArray[dataset]);
	var numOfYears = yearMax - yearMin + 1;

	startYearSelect.options.length = 0;
	endYearSelect.options.length = 0;
	for (var i = 0; i < numOfYears; i++)
	{
		var yearString = new String(i + yearMin);
		startYearSelect.options[i] = new Option(yearString, yearString, false,
				false);
		endYearSelect.options[i] = new Option(yearString, yearString, false,
				false);
	}
	if (!startYear || startYear < yearMin)
		startYearIndex = 0;
	else
		startYearIndex = startYear - yearMin;
	
	if (!endYear || endYear > yearMax)
		endYearIndex = numOfYears -1;
	else
		endYearIndex = endYear - yearMin;
	
	startYearSelect.selectedIndex = startYearIndex;
	endYearSelect.selectedIndex = endYearIndex;
}

/**
 * Reset the startYear/endYear select options
 * 
 * If the oldDataset == newDataset, it just sets startYearIndex=0 and
 * endYearIndex=length-1.
 * 
 * It also preserves the previously selected startYear and endYear if they are
 * within the range of the min and max of the new dataSet.
 */
function changeStartEndYears(newDataset)
{
	var startYearSelect = document.getElementById(startYearSelectId);
	var endYearSelect = document.getElementById(endYearSelectId);

	/* Start: Get all the fields and values --------------------- */
	var newYearMin = Number(startYearJSArray[newDataset]);
	var newYearMax = Number(endYearJSArray[newDataset]);
	var numOfYears = newYearMax - newYearMin + 1;
	/* End: Get all the fields and values ----------------------- */
	/*
	 * These are current values set in those select fields. They may not be the
	 * values of the oldDataset
	 */
	var oldYearMin = startYearSelect.options[0].value;
	var oldYearLen = startYearSelect.options.length;
	var oldYearMax = startYearSelect.options[oldYearLen - 1].value;

	// Detect the case of no change, just in case.
	if (oldYearMin == newYearMin && oldYearMax == newYearMax) return;

	/* Start: Recover the current values ------------------------ */
	var currentStartYearIndex = startYearSelect.selectedIndex;
	var currentEndYearIndex = endYearSelect.selectedIndex;
	/* Start: Recover the current values ------------------------ */

	/* Start: Reset start/endYearSelect options ----------------- */
	startYearSelect.options.length = 0;
	endYearSelect.options.length = 0;
	for (var i = 0; i < numOfYears; i++)
	{
		var yearString = new String(i + newYearMin);
		startYearSelect.options[i] = new Option(yearString, yearString, false,
				false);
		endYearSelect.options[i] = new Option(yearString, yearString, false,
				false);
	}
	/* End: Reset start/endYearSelect options ----------------- */

	/* Start: Reset start/endYearSelect selectedIndex --------- */
	/*
	 * If nothing was selected before, reset them with
	 * startYearSelect.selectedIndex=0 and
	 * endYearSelect.selectedIndex=numOfYears -1. Else, we need to copy the old
	 * indices but need to offset them.
	 */
	var startYearOffset = newYearMin - oldYearMin;
	var newStartYearIndex = currentStartYearIndex - startYearOffset;
	var newEndYearIndex = currentEndYearIndex - startYearOffset;

	// Set the new startYear
	if (newStartYearIndex < 0)
		newStartYearIndex = 0;
	else
		if (newStartYearIndex > (numOfYears - 1))
			newStartYearIndex = numOfYears - 1;

	if (newEndYearIndex < 0)
		newEndYearIndex = 0;
	else
		if (newEndYearIndex > (numOfYears - 1))
			newEndYearIndex = numOfYears - 1;

	if (newStartYearIndex > newEndYearIndex)
		newStartYearIndex = newEndYearIndex;

	startYearSelect.selectedIndex = newStartYearIndex;
	endYearSelect.selectedIndex = newEndYearIndex;
	/* End: Reset start/endYearSelect selectedIndex ----------- */
}