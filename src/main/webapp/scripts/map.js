/**
 * map.js Copyright CLIMDEX 2011-2012
 * 
 * Main part of this code follows the google-v3.html example provided by
 * OpenLayers 2.10 package. Some part of the code was taken from the
 * spherical-mercator.html example.
 * 
 * The code for Controls was found elsewhere on the Web. scalebar-fat.css was
 * taken from the same Web site.
 * 
 * The Feature request code was taken from the GeoServer LayerPreview GUI.
 * 
 * @see http://trac.osgeo.org/openlayers/wiki/GetFeatureInfo
 * 
 * TODO: Change the Layers to be tiled and using a buffer.
 */

"user strict";

// pink tile avoidance
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 5;
// make OL compute scale according to WMS spec
OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;

// The layer comes from the same serve as this web page.
// So there is no need for Proxy to make URL request
// The same cannot be said for the OSM layer
var protocol = window.location.protocol;
var host = window.location.host;

var wmsUrl = protocol + "//" + host + "/geoserver/wms";
var gwcUrl = protocol + "//" + host + "/geoserver/gwc/service/wms";
var wfsUrl = protocol + "//" + host + "/geoserver/wfs";

// This stores the current stations layer
var currentStationsLayer = "";
// This reference is needed to swap stations layer
var currentClickCtl = "";

var geographic = new OpenLayers.Projection("EPSG:4326");
var mercator = new OpenLayers.Projection("EPSG:900913");
var map, highlightLayer;

// This creates a new map, layers and a click control (WMSGetFeatureInfo)
function createMap(mapCenter, mapZoom, currentDataset)
{
	var world = new OpenLayers.Bounds(-179.983, -90, 179.32, 82.517).transform(geographic,mercator);	
	var marcatorBounds = new OpenLayers.Bounds(-20037508.34, -20037508.34,
            20037508.34, 20037508.34);
	var overviewMapCtl = new OpenLayers.Control.OverviewMap();
	var scaleLineCtl = new OpenLayers.Control.ScaleLine();
	var scaleCtl = new OpenLayers.Control.Scale();
	var mouseLocDiv = document.getElementById("mouseloc");
	var mouseLocCtl = new OpenLayers.Control.MousePosition(
	{
		'div' : mouseLocDiv,
		'autoActivate' : true
	});
	var navToolBarCtl = new OpenLayers.Control.NavToolbar();
	var panZoomBarCtl = new OpenLayers.Control.PanZoomBar();
	panZoomBarCtl.zoomWorldIcon = true;
	panZoomBarCtl.zoomStopHeight = 5;
	var keybCtl = new OpenLayers.Control.KeyboardDefaults();
	// var layerSwCtl = new OpenLayers.Control.LayerSwitcher();

	// OSM has only 19 levels
	var mapOptions =
	{
		projection : mercator,
		displayProjection : geographic,
		units : "m",
		numZoomLevels : 19,
		minResolution : "auto",
		maxResolution : "auto",
		maxExtent : world,
		controls : [ overviewMapCtl, scaleLineCtl, scaleCtl, mouseLocCtl,
				navToolBarCtl, panZoomBarCtl, keybCtl ]
	};

	map = new OpenLayers.Map('map', mapOptions);

	var baseLayer = new OpenLayers.Layer.OSM();
	map.addLayer(baseLayer);

	// This makes the stations layer for current dataSet
	// When dataSet is changed, this layer will be replaced
	var overlayLayers = [];
	var stationsLayer;

	var dataSets = Object.keys(datasetNameJSArray);

	var dataSet;
	for (var i = 0; i < dataSets.length; i++)
	{
		dataSet = dataSets[i];
		stationsLayer = makeStationsLayer(dataSet);
		if (dataSet == currentDataset)
			stationsLayer.setVisibility(true);
		else
			stationsLayer.setVisibility(false);
		currentStationsLayer = stationsLayer;
		map.addLayer(stationsLayer);
		overlayLayers.push(stationsLayer);
	}

	highlightLayer = new OpenLayers.Layer.Vector("selected",
	{
		displayInLayerSwitcher : true,
		isBaseLayer : false,
	});

	map.addLayer(highlightLayer);

	var clickControl = makeClickControl(overlayLayers);
	clickControl.events.register("getfeatureinfo", this, showInfo);

	currentClickCtl = clickControl;

	map.addControl(clickControl);
	clickControl.activate();

	// mapCenter and mapZoom are global properties
	var centerLonLat = OpenLayers.LonLat.fromString(mapCenter);
	if (mapCenter != "" && mapZoom != "")
	{
		map.setCenter(centerLonLat, mapZoom);
	}
	else
		if (!map.getCenter())
		{
			map.zoomToMaxExtent();
		}
	
    var click = new OpenLayers.Control.Click();
    map.addControl(click);
    click.activate();
}

/*
 * layer names and styles are defined in applicationCotnect.xml 
 * and passed to the JSP/scripts via Spring3/Struts2.
 */
function makeStationsLayer(dataSet)
{
	var layerWMSName = layerNameJSArray[dataSet];
	var styleWMSName = styleNameJSArray[dataSet];

	// The main URL must be WMS or WMSGetFeatureInfo does not work.
	// WCS is much faster but they are left in the layerUrls (optional URLs).
	var layerParams =
	{
		name : dataSet,
		layers : layerWMSName,
		url : wmsUrl,
		layerUrls : [ gwcUrl, gwcUrl, gwcUrl ],
		styles : styleWMSName,
		transparent : true
	};
	var layerOptions =
	{
		visibility : true,
		isBaseLayer : false,
		wrapDateLine : false
	};
	// Using multiple server connections is faster when tiles are used
	// Usually browsers allow 3 concurrent connections.
	var stationsLayer = new OpenLayers.Layer.WMS(dataSet, [ wmsUrl, wmsUrl, wmsUrl ], layerParams,
			layerOptions);

	return stationsLayer;
}

function makeClickControl(overlayLayers)
{
	var clickControl;

	var gmlFormat = new OpenLayers.Format.GML();
	
	// The main URL must be WMS or WMSGetFeatureInfo does not work.
	// WCS is much faster but they are left in the layerUrls (optional URLs).
	clickControl = new OpenLayers.Control.WMSGetFeatureInfo(
	{
		url : wmsUrl,
		layerUrls : [gwcUrl, gwcUrl, gwcUrl],
		title : 'selected station',
		maxFeatures : 1,
		layers : overlayLayers,
		infoFormat : "application/vnd.ogc.gml",
		format : gmlFormat,
		queryVisible : true
	});

	return clickControl;
}

/**
 * showInfo is called by the 'click' Control. It adds the feature to the
 * highlight and shows attributes on the HTML.
 */
function showInfo(evt)
{
	// First, select the feature o the map
	highlightLayer.destroyFeatures();
	var features = evt.features;
	var feature = null;
	if (features && features.length)
	{
		feature = features[0];
		var geometry = feature.geometry.clone();
		geometry.transform(geographic, mercator);
		feature.geometry = geometry;

		highlightLayer.addFeatures(features);
	}
	highlightLayer.redraw();
	// Then, update attributes on the table
	showAttributes(feature);
}

function showAttributes(feature)
{
	// If feature == null, this puts blanks into the fields
	var attributes = (feature) ? feature.attributes : null;

	var stationId = (feature) ? attributes.ID : "";
	var stationName = (feature) ? attributes.NAME : "";
	var latitude = (feature) ? attributes.LATITUDE : "";
	var longitude = (feature) ? attributes.LONGITUDE : "";
	var altitude = (feature) ? attributes.ALTITUDE : "";
	var active = (feature) ? attributes.ACTIVE : "";

	document.getElementById("myForm_stationdexRequest_stationId").value = stationId;
	document.getElementById("myForm_stationdexRequest_stationName").value = stationName;
	document.getElementById("myForm_stationdexRequest_latitude").value = latitude;
	document.getElementById("myForm_stationdexRequest_longitude").value = longitude;
	document.getElementById("myForm_stationdexRequest_altitude").value = altitude;
	var openData = "";
	if (active)
	{
		openData = (active == "1") ? "Yes" : "No";
	}

	document.getElementById("myForm_stationdexRequest_active").value = openData;

	// hidden element
	document.getElementById("myForm_stationdexRequest_mapCenter").value = map
			.getCenter().toShortString();
	// hidden element
	document.getElementById("myForm_stationdexRequest_mapZoom").value = map
			.getZoom();
}

// If oldDataset is null, this should not be called
function switchVisibleLayer(oldDataset, newDataset)
{
	clearErrMessage();
	// First, remove the current selections
	showAttributes(null);
	highlightLayer.destroyFeatures();
	highlightLayer.redraw();

	// Layers have been registered by names with the map
	var matchedLayers;
	if (oldDataset && oldDataset != newDataset)
	{
		matchedLayers = map.getLayersByName(oldDataset);
		var oldLayer = matchedLayers[0];
		oldLayer.setVisibility(false);
	}
	matchedLayers = map.getLayersByName(newDataset);
	var nextLayer = matchedLayers[0];
	nextLayer.setVisibility(true);
}

/** This click handler simply clears the error message if it exists */
OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {                
    defaultHandlerOptions: {
        'single': true,
        'double': false,
        'pixelTolerance': 0,
        'stopSingle': false,
        'stopDouble': false
    },

    initialize: function(options) {
        this.handlerOptions = OpenLayers.Util.extend(
            {}, this.defaultHandlerOptions
        );
        OpenLayers.Control.prototype.initialize.apply(
            this, arguments
        ); 
        this.handler = new OpenLayers.Handler.Click(
            this, {
                'click': this.trigger
            }, this.handlerOptions
        );
    }, 

    trigger: function(e) {
    	clearErrMessage();
    }
});
