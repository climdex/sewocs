/*
 * checkDataSource.js
 * 
 * previousDataset is defined and initialised by map.jsp as a global variable.
 * If there is only one dataset, this function should not be called.
 */
function checkDataSource(dataSourceSelect)
{
	clearErrMessage();
	
	var selectedIndex = dataSourceSelect.selectedIndex;
	var newDataset = dataSourceSelect.options[selectedIndex].value;
	var oldDataset = currentDataset;
	currentDataset = newDataset;
	// This if statement is a provision to avoid an error.
	if (oldDataset != newDataset)
	{
		changeStartEndYears(newDataset);
		switchVisibleLayer(oldDataset, newDataset);
	}
	return true;
}