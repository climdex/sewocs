-- mysql schema
create database sewocs;

use sewocs;

DROP TABLE IF EXISTS `ghcndStation`;
CREATE TABLE `ghcndStation` (
  `stationId` varchar(12) NOT NULL,
  `countryCode` varchar(2) DEFAULT NULL,
  `latitude` varchar(8) DEFAULT NULL,
  `longitude` varchar(9) DEFAULT NULL,
  `altitude` varchar(6) DEFAULT NULL,
  `usState` varchar(2) DEFAULT NULL,
  `stationName` varchar(30) DEFAULT NULL,
  `gsn` varchar(3) DEFAULT NULL,
  `hcn` varchar(3) DEFAULT NULL,
  `wmoid` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

create user climdexdba@localhost identified by 'xxxxxxxxxx';
grant all privileges on sewocs.* to climdexdba@localhost;

create user climdex@localhost identified by 'xxxxxxxxxx';
grant all privileges on sewocs.* to climdex@localhost;