/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.sewocs.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * StationdexRequest options for the HTML FORM SELECT. It needs to be put into
 * KeyValueElement in order for Struts tags to parse.
 * 
 * List<KeyValueElement> are filled from SewocsOptions, which is populated by
 * applicationContext.xml.
 * 
 * Values for dataSourceList, startYearList and endYearList will be reset by
 * JavaScript on the JSP page using DatasetOptions. This class, however, still
 * needs to supply dummy values.
 * 
 * @author yoichi
 * 
 */
public class StationdexOptions {
	private SewocsOptions sewocsOptions;
	
	private List<KeyValueElement> dataSourceList;
	private List<KeyValueElement> climateIndexList;
	private List<String> startYearList;
	private List<String> endYearList;
	private List<KeyValueElement> seasonList;
	private List<KeyValueElement> outputTypeList;

	/**
	 * Constructor must be an empty constructor
	 */
	public StationdexOptions() {
	}

	public SewocsOptions getSewocsOptions() {
		return sewocsOptions;
	}

	public void setSewocsOptions(SewocsOptions sewocsOptions) {
		this.sewocsOptions = sewocsOptions;
	}

	public List<KeyValueElement> getDataSourceList() {
		dataSourceList = new ArrayList<KeyValueElement>();
		Map<String, String> dataSourceMap = this.sewocsOptions.getDataSourceMap();
		for (Map.Entry<String, String> entry : dataSourceMap.entrySet()) {
			dataSourceList.add(new KeyValueElement(entry.getKey(), entry
					.getValue()));
		}
		return dataSourceList;
	}

	public void setDataSourceList(List<KeyValueElement> dataSourceList) {
		this.dataSourceList = dataSourceList;
	}

	public List<KeyValueElement> getClimateIndexList() {
		climateIndexList = new ArrayList<KeyValueElement>();

		Map<String, String> climateIndexMap = sewocsOptions
				.getClimateIndexMap();
		for (Map.Entry<String, String> entry : climateIndexMap.entrySet()) {
			climateIndexList.add(new KeyValueElement(entry.getKey(), entry
					.getValue()));
		}
		return climateIndexList;
	}

	public void setClimateIndexList(List<KeyValueElement> climateIndexList) {
		this.climateIndexList = climateIndexList;
	}

	public List<String> getStartYearList() {
		startYearList = new ArrayList<String>();
		startYearList.add("0");
		return startYearList;
	}

	public void setStartYearList(List<String> startYearList) {
		this.startYearList = startYearList;
	}

	public List<String> getEndYearList() {
		endYearList = new ArrayList<String>();
		endYearList.add("0");
		return endYearList;
	}

	public void setEndYearList(List<String> endYearList) {
		this.endYearList = endYearList;
	}

	public List<KeyValueElement> getSeasonList() {
		seasonList = new ArrayList<KeyValueElement>();

		Map<String, String> seasonMap = sewocsOptions.getSeasonMap();
		for (Map.Entry<String, String> entry : seasonMap.entrySet()) {
			seasonList
					.add(new KeyValueElement(entry.getKey(), entry.getValue()));
		}
		return seasonList;
	}

	public void setSeasonList(List<KeyValueElement> seasonList) {
		this.seasonList = seasonList;
	}

	public List<KeyValueElement> getOutputTypeList() {
		outputTypeList = new ArrayList<KeyValueElement>();

		Map<String, String> outputTypeMap = sewocsOptions.getOutputTypeMap();
		for (Map.Entry<String, String> entry : outputTypeMap.entrySet()) {
			outputTypeList.add(new KeyValueElement(entry.getKey(), entry
					.getValue()));
		}
		return outputTypeList;
	}

	public void setOutputTypeList(List<KeyValueElement> outputTypeList) {
		this.outputTypeList = outputTypeList;
	}
}
