/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.sewocs.model;

import java.util.Map;

/**
 * This is a bean to hold initial options settings
 * which is not affected by dataset choices.
 * 
 * This bean is used to create KeyValue lists.
 * 
 * The KeyValue list is sent with a JSP page to populate &lt;form&gt;&lt;select&gt;&lt;options&gt;.
 * 
 * @author yoichi
 * 
 */
public class SewocsOptions
{
	private Map<String,String> dataSourceMap;
	private Map<String,String> climateIndexMap;
	private Map<String,String> seasonMap;
	private Map<String,String> outputTypeMap;
	
	public Map<String, String> getDataSourceMap() {
		return dataSourceMap;
	}
	public void setDataSourceMap(Map<String, String> dataSourceMap) {
		this.dataSourceMap = dataSourceMap;
	}
	public Map<String, String> getClimateIndexMap() {
		return climateIndexMap;
	}
	public void setClimateIndexMap(Map<String, String> climateIndexMap) {
		this.climateIndexMap = climateIndexMap;
	}
	public Map<String, String> getSeasonMap() {
		return seasonMap;
	}
	public void setSeasonMap(Map<String, String> seasonMap) {
		this.seasonMap = seasonMap;
	}
	public Map<String, String> getOutputTypeMap() {
		return outputTypeMap;
	}
	public void setOutputTypeMap(Map<String, String> outputTypeMap) {
		this.outputTypeMap = outputTypeMap;
	}
}
