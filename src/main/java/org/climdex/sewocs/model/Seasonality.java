package org.climdex.sewocs.model;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This Class is not used yet. These values are hard coded in the checkSeason.js
 * 
 * @author yoichi
 *
 */
public class Seasonality
{
	private LinkedHashMap<String, String> seasonalityMap;
	
	public Seasonality()
	{
	}
	
	private void initSeasonalityMap() {
		this.seasonalityMap = new LinkedHashMap<String, String>();
		seasonalityMap.put("FD", "annualOnly");
		seasonalityMap.put("SU", "annualOnly");
		seasonalityMap.put("ID", "annualOnly");
		seasonalityMap.put("TR", "annualOnly");
		seasonalityMap.put("GSL", "annualOnly");
		seasonalityMap.put("TXx", "seasonable");
		seasonalityMap.put("TNx", "seasonable");
		seasonalityMap.put("TXn", "seasonable");
		seasonalityMap.put("TNn", "seasonable");
		seasonalityMap.put("TN10p", "seasonable");
		seasonalityMap.put("TX10p", "seasonable");
		seasonalityMap.put("TN90p", "seasonable");
		seasonalityMap.put("TX90p", "seasonable");
		seasonalityMap.put("WSDI", "annualOnly");
		seasonalityMap.put("CSDI", "annualOnly");
		seasonalityMap.put("DTR", "seasonable");
		seasonalityMap.put("Rx1day", "seasonable");
		seasonalityMap.put("Rx5day", "seasonable");
		seasonalityMap.put("SDII", "annualOnly");
		seasonalityMap.put("R10mm", "annualOnly");
		seasonalityMap.put("R20mm", "annualOnly");
		seasonalityMap.put("Rnn", "annualOnly");
		seasonalityMap.put("CDD", "annualOnly");
		seasonalityMap.put("CWD", "annualOnly");
		seasonalityMap.put("R95p", "annualOnly");
		seasonalityMap.put("R99p", "annualOnly");
		seasonalityMap.put("PRCPTOT", "annualOnly");	
	}
	
	@Autowired
	public String getSeasonalityJSArray() throws JsonGenerationException, JsonMappingException, IOException
	{
		initSeasonalityMap();
		ObjectMapper mapper = new ObjectMapper();
		String SeasonalityListJSString = mapper.writeValueAsString(this.seasonalityMap);
		return SeasonalityListJSString;
	}
}
