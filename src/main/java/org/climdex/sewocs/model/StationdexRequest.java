package org.climdex.sewocs.model;

public class StationdexRequest
{
	private String stationId = null;
	private String stationName = null;
	private String latitude = null;
	private String longitude = null;
	private String altitude = null;
	private String active = null;
	private String dataSource = null;
	private String climateIndex = null;
	private String startYear = null; // this will be overridden by datasetOptions
	private String endYear = null; // this will be overridden by datasetOptions
	private String season = null;
	private String outputType = null;
	/**
	 * empty mapCenter or mapZoom will get map.zoomToMaxExtent().
	 * 
	 * var centerlonLat = centerLonLatOpenLayers.LonLat.fromString(mapCenter);
	 */
	private String mapCenter = "";
	/**
	 * empty mapCenter or mapZoom will get map.zoomToMaxExtent()
	 */
	private String mapZoom = "";

	public String getStationId()
	{
		return stationId;
	}

	public void setStationId(String stationId)
	{
		this.stationId = stationId;
	}

	public String getStationName()
	{
		return stationName;
	}

	public void setStationName(String stationName)
	{
		this.stationName = stationName;
	}

	public String getLatitude()
	{
		return latitude;
	}

	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getAltitude()
	{
		return altitude;
	}

	public void setAltitude(String altitude)
	{
		this.altitude = altitude;
	}

	public String getActive()
	{
		return active;
	}

	public void setActive(String active)
	{
		this.active = active;
	}

	public String getDataSource()
	{
		return dataSource;
	}

	public void setDataSource(String dataSource)
	{
		this.dataSource = dataSource;
	}

	public String getClimateIndex()
	{
		return climateIndex;
	}

	public void setClimateIndex(String climateIndex)
	{
		this.climateIndex = climateIndex;
	}

	public String getStartYear()
	{
		return startYear;
	}

	public void setStartYear(String startYear)
	{
		this.startYear = startYear;
	}

	public String getEndYear()
	{
		return endYear;
	}

	public void setEndYear(String endYear)
	{
		this.endYear = endYear;
	}

	public String getSeason()
	{
		return season;
	}

	public void setSeason(String season)
	{
		this.season = season;
	}

	public String getOutputType()
	{
		return outputType;
	}

	public void setOutputType(String outputType)
	{
		this.outputType = outputType;
	}

	public String getMapCenter()
	{
		return mapCenter;
	}

	public void setMapCenter(String mapCenter)
	{
		this.mapCenter = mapCenter;
	}

	public String getMapZoom()
	{
		return mapZoom;
	}

	public void setMapZoom(String mapZoom)
	{
		this.mapZoom = mapZoom;
	}
}
