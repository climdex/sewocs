package org.climdex.sewocs.service.dao;

public class RException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RException(String msg)
	{
		super("R error: \"" + msg + "\"");
	}
}
