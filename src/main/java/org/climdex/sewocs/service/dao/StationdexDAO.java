package org.climdex.sewocs.service.dao;

public interface StationdexDAO
{
	abstract byte[] getStationIndexPlot(String dataDir, String dataSource,
			String stationId, String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws DAOException;

	abstract double[][] getStationdexASCII(String dataDir, String dataSource,
			String stationId, String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws DAOException;
}
