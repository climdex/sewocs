/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. 
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 	
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.

 * CLIMDEX Project Group provides access to this code for bona fide
 * scientific research and personal usage only.

 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow 
 * researchers. Everyone is welcome to use the code, and we aim to make it 
 * reliable, but it does not have the same level of support as an operational 
 * service.
 */
package org.climdex.sewocs.ghcn;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileView;

/**
 * <P>
 * GHCNDEXStationListDelimitByTabs.java
 * </p>
 * <p>
 * This program reads the all-stations.txt of the GHCNDEX distribution of the
 * GHCN-Daily, and converts it to a TAB delimited file with a header that
 * contains column names.
 * </p>
 * <p>
 * You can locate the input file using a GUI. You can also name the output file
 * arbitrary.
 * </p>
 * <P>
 * -999 is replaced as an empty field. Unused fields are removed. This is to put
 * it in the same format as those of the other dataSets, e.g., HADEX2.
 * </P>
 * <P>
 * The file is suitable to be used as an input for QGIS, which requires a
 * tab-delimited Text file, in order to create a map. The map is output as a
 * Shapefile, which is then used by GeoServer.
 * </P>
 * 
 * @author yoichi2
 * @deprecated Use Fixed2Shape.java (another independent Eclipse Java project)
 */
public class GHCNDEXStationListDelimitByTabs extends JPanel implements
		ActionListener
{
	/**
	 * JFrame and FileChooser member variables
	 */
	private static final long serialVersionUID = 1L;
	static private String newline = "\n";
	private JScrollPane msgScrollPane;
	private JTextArea msgTextArea;
	private GHCNDEXStationListDelimitByTabs parentContainer;
	private JFileChooser fileChooser;
	private File inputFile = null;
	private File outputFile = null;
	/**
	 * t ConvCols2Tabs member varialbes
	 */
	private final static String DELIM = "\t";
	private final static String LF = "\n";
	private static final String ACTIVE = "1"; // All station data are available
	private static final String DEFAULT_DIRECTORY_PATH_STR = "";
	private static final String DEFAULT_OUTPUTFILE_STR = "all-stations.tab";

	public GHCNDEXStationListDelimitByTabs()
	{
		super(new BorderLayout());

		// Create the message pane first, because the action listener
		// needs to refer to it.
		String msg = "This Java utility delimts columns of a text file using TABs."
				+ newline + newline;
		msgTextArea = new JTextArea(msg, 5, 20);
		msgTextArea.setLineWrap(true);
		msgTextArea.setMargin(new Insets(5, 5, 5, 5));
		msgTextArea.setEditable(false);
		msgScrollPane = new JScrollPane(msgTextArea);

		JButton fileChooserButton = new JButton(
				"Click here to select an input file...");
		fileChooserButton.addActionListener(this);

		add(msgScrollPane, BorderLayout.CENTER);
		add(fileChooserButton, BorderLayout.PAGE_END);
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		/*
		 * First, get the input file name
		 */
		inputFile = null;
		// Set up the file chooser.
		if (fileChooser == null)
		{
			fileChooser = new JFileChooser();

			// Add a custom file filter and disable the default
			// (Accept All) file filter.
			// fc.addChoosableFileFilter(new ImageFileFilter());
			FileNameExtensionFilter filter = new FileNameExtensionFilter(
					"TEXT FILES", "txt", "text");
			fileChooser.setFileFilter(filter);
			fileChooser.setAcceptAllFileFilterUsed(false);

			// Add custom icons for file types.
			// fc.setFileView(new TextView());

			// Add the preview pane.
			// fc.setAccessory(new TextPreview(fc));
		}

		// Show it.
		parentContainer = GHCNDEXStationListDelimitByTabs.this;
		int returnVal = fileChooser.showDialog(parentContainer, "Select");

		// Process the results.
		if (returnVal == JFileChooser.APPROVE_OPTION)
		{
			inputFile = fileChooser.getSelectedFile();
			msgTextArea.append("Selected file: " + inputFile.getName() + "."
					+ newline);
		}
		else
		{
			msgTextArea.append("Selecting cancelled by user." + newline);
		}
		msgTextArea.setCaretPosition(msgTextArea.getDocument().getLength());

		// Reset the file chooser for the next time it's shown.
		fileChooser.setSelectedFile(null);

		if (inputFile == null) return;

		/*
		 * Next, choose the output file
		 */
		fileChooser.setDialogTitle("Specify a file to save");
		fileChooser.setCurrentDirectory(new File(DEFAULT_DIRECTORY_PATH_STR));
		fileChooser.setSelectedFile(new File(DEFAULT_OUTPUTFILE_STR));
		int userSelection = fileChooser.showSaveDialog(parentContainer);

		if (userSelection == JFileChooser.APPROVE_OPTION)
		{
			outputFile = fileChooser.getSelectedFile();
			msgTextArea.append("Output file: " + outputFile.getAbsolutePath()
					+ newline);
		}

		/*
		 * Update the msgPanel, because it won't while it is doing the processFile()
		 */
		parentContainer.validate();

		/*
		 * If all is well, proceed to process
		 */
		if (inputFile != null && outputFile != null) processFile();
	}

	/**
	 * Core processing of the file
	 * 
	 * @param file
	 */
	public void processFile()
	{
		File stationFile = inputFile;
		FileReader fr = null;
		BufferedReader br = null;

		File delimtedFile = outputFile;
		if (delimtedFile.exists()) delimtedFile.delete();

		FileWriter fw = null;
		BufferedWriter bw = null;
		String err = null;
		
		msgTextArea.append(newline);
		try
		{
			fr = new FileReader(stationFile);
			br = new BufferedReader(fr);

			fw = new FileWriter(delimtedFile);
			bw = new BufferedWriter(fw);

			String stationId, countryCode, latitude, longitude, altitude;
			String usState, stationName, gsn, hcn, wmoid;
			String line = null;
			String outline = null;
			int i = 0;
			while ((line = br.readLine()) != null)
			{
				i++;
				msgTextArea.append("line=" + i + newline);
				parentContainer.validate();
				System.out.println("line=" + i);

				if (i == 1)
				{
					outline = "ID\tLATITUDE\tLONGITUDE\tALTITUDE\tNAME\tACTIVE\n";
				}
				else
				{
					stationId = line.substring(0, 11).trim(); // index 0 to 10
					countryCode = line.substring(0, 2).trim(); // index 0 & 1
					latitude = line.substring(12, 20).trim(); // index 12 to 19
					longitude = line.substring(21, 30).trim();
					altitude = line.substring(31, 37).trim();
					usState = line.substring(38, 40).trim();
					stationName = line.substring(41, 71).trim();
					gsn = line.substring(72, 75).trim();
					hcn = line.substring(76, 79).trim();
					// wmoid = line.substring(80, 85).trim();
					wmoid = line.substring(80).trim(); // 80-85 but some lines
														// are shorter than 85

					if (latitude.contains("-999")) latitude = "";
					if (longitude.contains("-999")) longitude = "";
					if (altitude.contains("-999")) altitude = "";

					outline = stationId + DELIM;
					outline += latitude + DELIM;
					outline += longitude + DELIM;
					outline += altitude + DELIM;
					outline += stationName + DELIM;
					outline += ACTIVE + LF;
				}

				bw.write(outline);
				bw.flush();
			}
			bw.flush();
			
			msgTextArea.append(newline + "Precessing finished normally." + newline);
			parentContainer.validate();
		}
		catch (Exception e)
		{
			err = e.getLocalizedMessage() + newline;
			msgTextArea.append(err);
			parentContainer.validate();
		}
		finally
		{
			if (br != null) try
			{
				br.close();
			}
			catch (IOException e1)
			{
				err = e1.getLocalizedMessage() + newline;
				msgTextArea.append(err);
				parentContainer.validate();
			}
			if (bw != null) try
			{
				bw.close();
			}
			catch (IOException e2)
			{
				err = e2.getLocalizedMessage() + newline;
				msgTextArea.append(err);
				parentContainer.validate();
			}
			String msg = newline
					+ "Please close the application Window when you are finished.";
			msgTextArea.append(msg);
			parentContainer.validate();
		}
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event dispatch thread.
	 */
	private static void createAndShowGUI()
	{
		// Create and set up the window.
		JFrame frame = new JFrame("Delimit columns by TABs");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add content to the window.
		frame.add(new GHCNDEXStationListDelimitByTabs());

		// Display the window.
		frame.pack();
		// frame.setBounds(300,300,350,100);
		int x = 300;
		int y = 300;
		int w = 500;
		int h = 200;
		frame.setBounds(x, y, w, h);
		frame.setVisible(true);
	}

	public static void main(String[] args)
	{
		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				// Turn off metal's use of bold fonts
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();
			}
		});
	}
}
