package org.climdex.sewocs.hadex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.climdex.sewocs.model.WeatherStation;

/**
 * Merging program for HADEX2 station files.
 * 
 * Run SortStationsLists.java first to sort them in the stationId order. Then,
 * this class can read in sorted station lists and merge them into one file.
 * 
 * The resulting file MERGE_FILE_NAME is then used to produce Mapfile for HADEX2
 * stations
 * 
 * @link {http://www.unidata.ucar.edu/software/netcdf-java/tutorial/}
 * @author yoichi
 * 
 */
public class MergeSortedStationLists
{
	private final static String DELIM = "\t";
	private final static String LF = "\n";
	private final static String[] INDICES = { "TXx", "TNx", "TXn", "TNn",
			"TN10p", "TX10p", "TN90p", "TX90p", "DTR", "GSL", "ID", "FD", "SU",
			"TR", "WSDI", "CSDI", "Rx1day", "Rx5day", "SDII", "R10mm", "R20mm",
			"CDD", "CWD", "R95p", "R99p", "PRCPTOT" };
	private final static String DATADIR = "/scratch/climdex/hadex/HadEX2/current/stn-indices";
	private final static String INPUT_FILE_NAME = "HadEX2_station_list_all_INDEX_NAME.sorted.tab";
	private final static String MERGE_FILE_NAME = "HadEX2_station_list_all.tab";

	/**
	 * Each list has to be sorted by the stationId before they can be merged.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException
	{
		MergeSortedStationLists me = new MergeSortedStationLists();
		List<BufferedReader> brs = null;
		BufferedWriter bw = null;
		try
		{
			brs = me.openAllSortedFiles(); // it skips the header line
			bw = me.openMergeOutputFile(); // it writes the header line
			me.mergeFiles(brs, bw);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			for (BufferedReader br : brs)
			{
				br.close();
			}
			bw.close();
		}
		System.out.println("All processing finished");
	}

	private void mergeFiles(List<BufferedReader> brs, BufferedWriter bw)
			throws IOException
	{
		// headers are skipped already
		// read the first stations
		List<WeatherStation> nextStations = readFirstStations(brs);
		// find the minStation and read one more line from that br
		WeatherStation currentStation = findMinStation(nextStations, brs);

		while (currentStation != null)
		{
			bw = writeStation(bw, currentStation);
			currentStation = findMinStation(nextStations, brs);
		}
	}

	/**
	 * Find a station with min ID Side effect is that some brs will be advanced
	 * 
	 * @param nextStations
	 * @param brs
	 * @return
	 * @throws IOException
	 */
	private WeatherStation findMinStation(List<WeatherStation> nextStations,
			List<BufferedReader> brs) throws IOException
	{
		// First fine non null and min station
		WeatherStation minStation = null;
		for (WeatherStation station : nextStations)
		{
			if (station == null) continue;
			if (minStation == null)
				minStation = station;
			else
				if (minStation.compareTo(station) > 0) minStation = station;
		}

		int n = nextStations.size();
		for (int i = 0; i < n; i++)
		{
			WeatherStation station = nextStations.get(i);
			if (station == null) continue;
			if (minStation.compareTo(station) == 0)
			{
				BufferedReader br = brs.get(i);
				WeatherStation nextStation = readLineIntoStation(br, false);
				nextStations.set(i, nextStation);
			}
		}

		return minStation;
	}

	/**
	 * Read in first stations Side effect is that all brs will be advanced
	 * 
	 * @param brs
	 * @return
	 * @throws IOException
	 */
	private List<WeatherStation> readFirstStations(List<BufferedReader> brs)
			throws IOException
	{
		List<WeatherStation> stations = new ArrayList<WeatherStation>();
		WeatherStation station;
		boolean skip = false;
		for (BufferedReader br : brs)
		{
			station = readLineIntoStation(br, skip);
			stations.add(station);
		}
		return stations;
	}

	private BufferedWriter openMergeOutputFile() throws IOException
	{
		String outFileName = DATADIR + File.separator + MERGE_FILE_NAME;
		File of = new File(outFileName);
		FileWriter fw = new FileWriter(of);
		BufferedWriter bw = new BufferedWriter(fw);
		bw = writeHeader(bw);
		return bw;
	}

	private BufferedWriter writeHeader(BufferedWriter bw) throws IOException
	{
		// Shapefile uses all upper case names
		String line = "ID" + DELIM; // MAPFILE automatically uses "ID"
		line += "LATITUDE" + DELIM; // MAPFILE automatically uses "LATITUDE"
		line += "LONGITUDE" + DELIM; // MAPFILE automatically uses "LONGITUDE"
		line += "ALTITUDE" + DELIM; // MAPFILE automatically uses "ALTITUDE"
		line += "NAME" + DELIM; // MAPFILE automatically uses "NAME"
		//line += "COUNTRY" + DELIM;
		//line += "STARTYEAR" + DELIM;
		//line += "ENDYEAR" + DELIM;
		//line += "SOURCEDIR" + DELIM;
		line += "ACTIVE" + LF;

		// What indices it has may follow here

		bw.write(line);
		bw.flush();
		return bw;
	}

	private BufferedWriter writeStation(BufferedWriter bw,
			WeatherStation currentStation) throws IOException
	{
		String line = currentStation.getId() + DELIM;
		line += currentStation.getLatitude() + DELIM;
		line += currentStation.getLongitude() + DELIM;
		line += currentStation.getAltitude() + DELIM;
		line += currentStation.getName() + DELIM;
		//line += currentStation.getCountry() + DELIM;
		//line += currentStation.getStartYear() + DELIM;
		//line += currentStation.getEndYear() + DELIM;
		//line += currentStation.getSourceDir() + DELIM;
		line += currentStation.getActive() + LF;

		// What indices it has may follow here

		bw.write(line);
		bw.flush();
		return bw;
	}

	private List<BufferedReader> openAllSortedFiles() throws IOException
	{
		List<BufferedReader> brs = new ArrayList<BufferedReader>();
		String inputFilePath;
		for (String index : INDICES)
		{
			inputFilePath = DATADIR + File.separator + INPUT_FILE_NAME;
			inputFilePath = inputFilePath.replace("INDEX_NAME", index);
			FileReader fr = new FileReader(inputFilePath);
			BufferedReader br = new BufferedReader(fr);
			skipHeader(br);
			brs.add(br);
		}
		return brs;
	}

	private void skipHeader(BufferedReader br) throws IOException
	{
		boolean skip = true;
		readLineIntoStation(br, skip);
	}

	/**
	 * This read a modified station data. delimited by tab
	 * 
	 * @param br
	 * @param skip
	 * @return
	 * @throws IOException
	 */
	private WeatherStation readLineIntoStation(BufferedReader br, boolean skip)
			throws IOException
	{
		WeatherStation station = new WeatherStation();

		String stationId, latitude, longitude, altitude, index, stationName, country, region;
		String sourceId, source, sourceDir, startYear, endYear, dailyData, notes, active;

		String line = null;

		if (skip)
		{
			line = br.readLine(); // discard the first line
			return null;
		}

		line = br.readLine();
		if (line == null) return null;

		String[] columns = line.split(DELIM);
		int len = columns.length;
		if (len > 11)
		{
			System.out.println(line);
			throw new IndexOutOfBoundsException("len=" + len);
		}

		stationId = columns[0].trim().replace("\"", "");
		latitude = columns[1].trim().replace("\"", "");
		longitude = columns[2].trim().replace("\"", "");
		altitude = columns[3].trim().replace("\"", "");
		stationName = columns[4].trim().replace("\"", "");
		country = columns[5].trim().replace("\"", "");
		startYear = columns[6].trim().replace("\"", "");
		// there are some "" invisible characters
		try
		{
			Integer startYearInt = Integer.valueOf(startYear);
		}
		catch (NumberFormatException e)
		{
			startYear = "";
		}
		endYear = columns[7].trim().replace("\"", "");
		// there are some "" invisible characters
		try
		{
			Integer endYearInt = Integer.valueOf(endYear);
		}
		catch (NumberFormatException e)
		{
			endYear = "";
		}
		sourceDir = columns[8].trim().replace("\"", "");
		index = columns[9].trim().replace("\"", "");
		active = columns[10].trim().replace("\"", "");

		station.setId(stationId); // id is recognized by Shapefile
		station.setLatitude(latitude); // recognized by Shapefile
		station.setLongitude(longitude); // recognized by Shapefile
		station.setAltitude(altitude); // recognized by Shapefile
		station.setName(stationName); // ; name is recognized by Shapefile
		station.setCountry(country);
		station.setSourceDir(sourceDir);
		station.setStartYear(startYear);
		station.setEndYear(endYear);
		station.setIndex(index);
		station.setActive(active);

		return station;
	}
}
