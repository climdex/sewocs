package org.climdex.sewocs.hadex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.climdex.sewocs.model.WeatherStation;

/**
 * This program reads in station lists that are in csv format, sorts rows by
 * stationId and writes them out as tab delimited files. This is a preparation
 * to merging them.
 * 
 * @link {http://www.unidata.ucar.edu/software/netcdf-java/tutorial/}
 * @author yoichi
 * 
 */
public class SortStationLists
{
	private static final String COMMA = ",";
	private final static String TAB = "\t";
	private final static String LF = "\n";
	private final static String[] INDICES = { "TXx", "TNx", "TXn", "TNn",
			"TN10p", "TX10p", "TN90p", "TX90p", "DTR", "GSL", "ID", "FD", "SU",
			"TR", "WSDI", "CSDI", "Rx1day", "Rx5day", "SDII", "R10mm", "R20mm",
			"CDD", "CWD", "R95p", "R99p", "PRCPTOT" };
	private final static String DATADIR = "/scratch/climdex/hadex/HadEX2/current/stn-indices";
	private final static String INPUT_FILE_NAME = "HadEX2_station_list_all_INDEX_NAME.csv";
	private final static String OUTPUT_FILE_NAME = "HadEX2_station_list_all_INDEX_NAME.sorted.tab";

	/**
	 * Each list has to be sorted by the stationId.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException
	{
		SortStationLists me = new SortStationLists();
		BufferedReader br = null;
		List<WeatherStation> stations = null;
		try
		{
			for (String indexName : INDICES)
			{
				br = me.openIndexStationListFile(indexName);
				// skip the first header line
				boolean skip = true;
				me.readLineIntoStation(br, skip);
				stations = me.readAllStations(br);
				stations = me.sortStationList(stations);
				me.writeIndexStationListFile(indexName, stations);
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			br.close();
		}
		System.out.println("All processing finished");
	}

	/**
	 * Sort
	 * 
	 * @param stations
	 * @return
	 */
	private List<WeatherStation> sortStationList(List<WeatherStation> stations)
	{
		if (stations == null) return stations;
		int n = stations.size();
		if (n == 1) return stations;

		List<WeatherStation> sortedStations = new ArrayList<WeatherStation>(
				stations);
		Collections.sort(sortedStations);
		return sortedStations;
	}

	/**
	 * Read all stations into a list in memory
	 * 
	 * @param br
	 * @return
	 * @throws IOException
	 */
	private List<WeatherStation> readAllStations(BufferedReader br)
			throws IOException
	{
		List<WeatherStation> stations = new ArrayList<WeatherStation>();
		WeatherStation station = null;
		boolean skip = false;
		while ((station = readLineIntoStation(br, skip)) != null)
		{
			stations.add(station);
		}

		return stations;
	}

	private BufferedReader openIndexStationListFile(String indexName)
			throws FileNotFoundException
	{
		String fileName = DATADIR + File.separator + INPUT_FILE_NAME;
		fileName = fileName.replace("INDEX_NAME", indexName);
		FileReader fr = new FileReader(fileName);
		BufferedReader br = new BufferedReader(fr);
		return br;
	}

	private void writeIndexStationListFile(String indexName,
			List<WeatherStation> stations) throws IOException
	{
		String outFileName = DATADIR + File.separator + OUTPUT_FILE_NAME;
		outFileName = outFileName.replace("INDEX_NAME", indexName);
		File of = new File(outFileName);
		FileWriter fw = new FileWriter(of);
		BufferedWriter bw = new BufferedWriter(fw);

		// Header line
		String line = "ID" + TAB; // MAPFILE automatically uses "ID"
		line += "LATITUDE" + TAB; // MAPFILE automatically uses "LATITUDE"
		line += "LONGITUDE" + TAB; // MAPFILE automatically uses "LONGITUDE"
		line += "ALTITUDE" + TAB; // MAPFILE automatically uses "ALTITUDE"
		line += "NAME" + TAB; // MAPFILE automatically uses "NAME"
		line += "COUNTRY" + TAB;
		line += "STARTYEAR" + TAB;
		line += "ENDYEAR" + TAB;
		line += "SOURCEDIR" + TAB;
		line += "INDEXNAME" + TAB;
		line += "ACTIVE" + LF; // Whether the station data is public or private
		bw.write(line);
		bw.flush();

		// Write out stations
		for (WeatherStation station : stations)
		{
			writeOutStation(bw, station);
		}
		bw.flush();
		bw.close();
	}

	private WeatherStation readLineIntoStation(BufferedReader br, boolean skip)
			throws IOException
	{
		WeatherStation station = new WeatherStation();

		String stationId, latitude, longitude, altitude, index, stationName, country, region;
		String sourceId, source, sourceDir, startYear, endYear, dailyData, notes;

		String line = null;

		if (skip)
		{
			line = br.readLine(); // discard the first line
			return null;
		}

		line = br.readLine();

		if (line == null) return null;

		String[] columns = line.split(COMMA);
		int len = columns.length;
		if (len > 15)
		{
			System.out.println(line);
			throw new IndexOutOfBoundsException("len=" + len);
		}

		stationId = columns[0].trim().replace("\"", "");
		latitude = columns[1].trim().replace("\"", "");
		longitude = columns[2].trim().replace("\"", "");
		altitude = columns[3].trim().replace("\"", "");
		index = columns[4].trim().replace("\"", "");
		stationName = columns[5].trim().replace("\"", "");
		country = columns[6].trim().replace("\"", "");
		region = columns[7].trim().replace("\"", "");
		sourceId = columns[8].trim().replace("\"", "");
		source = columns[9].trim().replace("\"", "");
		sourceDir = columns[10].trim().replace("\"", "");
		startYear = columns[11].trim().replace("\"", "");
		// there are some "" invisible characters
		try
		{
			// Test whether the String is an integer value
			Integer startYearInt = Integer.valueOf(startYear);
		}
		catch (NumberFormatException e)
		{
			startYear = "";
		}
		endYear = columns[12].trim().replace("\"", "");
		// there are some "" invisible characters
		try
		{
			// Test whether the String is an integer value
			Integer endYearInt = Integer.valueOf(endYear);
		}
		catch (NumberFormatException e)
		{
			endYear = "";
		}
		dailyData = columns[13].trim().replace("\"", "");
		// some notes is empty and cannot access the columns[14]
		try
		{
			notes = columns[14].trim().replace("\"", "");
		}
		catch (Exception e)
		{
			notes = "";
		}

		station.setId(stationId); // id is recognized by Shapefile
		station.setLatitude(latitude); // recognized by Shapefile
		station.setLongitude(longitude); // recognized by Shapefile
		station.setAltitude(altitude); // recognized by Shapefile
		station.setIndex(index); // this is used to know which file
		station.setName(stationName); // ; name is recognized by Shapefile
		station.setCountry(country);
		station.setRegion(region);
		station.setSourceId(sourceId);
		station.setSource(source);
		station.setSourceDir(sourceDir);
		station.setStartYear(startYear);
		station.setEndYear(endYear);
		station.setDailyData(dailyData);
		station.setNotes(notes);

		station = setAvailability(station);

		return station;
	}

	public static final String[] PUBLIC_SOURCES = new String[] { "SACA",
			"Australia", "Brazil_INMET", "Congo_workshop", "ECAD2",
			"HadEX1_adds", "LACAD", "New_Zealand", "SACA", "South_Africa",
			"USA", "Vietnam_workshop", "West_Indian_ocean" };
	public static final Set<String> PUBLIC_SET = new HashSet<String>(
			Arrays.asList(PUBLIC_SOURCES));

	private WeatherStation setAvailability(WeatherStation station)
	{
		String sourceDir = station.getSourceDir();
		String active = "0";
		if (PUBLIC_SET.contains(sourceDir)) active = "1";
		station.setActive(active);
		return station;
	}

	private void writeOutStation(BufferedWriter bw, WeatherStation station)
			throws IOException
	{
		// Some of the station fields are not used
		String stationId, latitude, longitude, altitude, index, stationName, country, region;
		String sourceId, source, sourceDir, startYear, endYear, dailyData, notes, active;

		stationId = (String) station.getId();
		latitude = (String) station.getLatitude();
		longitude = (String) station.getLongitude();
		altitude = (String) station.getAltitude();
		index = (String) station.getIndex();
		stationName = (String) station.getName();
		country = (String) station.getCountry();
		startYear = (String) station.getStartYear();
		endYear = (String) station.getEndYear();
		sourceDir = (String) station.getSourceDir();
		active = (String) station.getActive();

		String line = "";
		line += stationId + TAB;
		line += latitude + TAB;
		line += longitude + TAB;
		line += altitude + TAB;
		line += stationName + TAB;
		line += country + TAB;
		line += startYear + TAB;
		line += endYear + TAB;
		line += sourceDir + TAB;
		line += index + TAB;
		line += active + LF;

		bw.write(line);
		bw.flush();
	}
}
