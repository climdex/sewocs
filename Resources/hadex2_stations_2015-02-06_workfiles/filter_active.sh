# script to filter through only lines for public domain stations.
# Public stations are indicated with the value=1 in ACTIVE column.
# The results are put into new and renamed TXT files.
#bin/sh
for f in *.tab
do
	txtfile="${f%.tab}.txt"
	renamed="HadEX2_public_station_list${txtfile#HadEX2_station_list_all}"
	echo "filtering $f to $renamed for ACTIVE=1"
	cat "$f" | grep "1$" > $renamed
done
