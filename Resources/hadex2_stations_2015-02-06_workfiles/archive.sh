# A script to archive only the pubic domain station data.
# The tar must recurse different CLIMDEX index directories and
# archive only files that are public domain.
# The exclusion list contains the patters of station names that
# are to be excluded (ACTIVE=0 in the station_list_all) from 
# any location. This is because the station name may happen in
# multiple locations, i.e., for different indices.
#
#!/bin/sh
nohup tar -czf HadEX2_public_stations_2015-02-06.tgz --exclude-from=exclusions.txt
