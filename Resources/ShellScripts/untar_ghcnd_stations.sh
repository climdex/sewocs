#!/bin/bash

# A simple shell script to untar all tar.gz files 
# in the stn-indices directory

cd /scratch/climdex/ghcndex/GHCNDEX_2012/ghcndex_Oct2012/stn-indices
FILES=*.tar.gz
for F in $FILES
do
  echo "untaring $F"
  tar -xzf $F
done
