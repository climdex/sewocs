#!/bin/bash
cd /scratch/climdex/ghcn/output
FILES=*.tar
for F in $FILES
do
  echo "Processing $F"
  STNID=${F%.tar}
  CNTRYID=${STNID:0:2}
  TARGET=../unarchived/$CNTRYID/$STNID
  echo "mkdir -p $TARGET"
  mkdir -p $TARGET
  echo "untaring $F into $TARGET"
  tar -xvf $F -C $TARGET
done